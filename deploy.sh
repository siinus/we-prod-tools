#/bin/bash
SECONDS=0

echo -e "\e[00;31mwe prod deploy script 1.0.1\e[00m"
source config.sh

rsync -a --delete --exclude '.git/'  --exclude 'var/' $LOCAL_FOLDER "$DEPLOY_FOLDER/" 
rsync -a  --exclude 'cache/' "$LOCAL_FOLDER/var/" "$DEPLOY_FOLDER/var" 

# priv cache update 1/2
PRIVPOOLCACHEFILE="$DEPLOY_FOLDER/var/cache/we-base-priv-pool.serialized"
PRIVPOOLCACHESHA=''
if [  -f $PRIVPOOLCACHEFILE ]; then
	PRIVPOOLCACHESHA="$(sha1sum $PRIVPOOLCACHEFILE)"
fi

rm -rf "$DEPLOY_FOLDER/var/cache/*"
rsync -a --delete  "$LOCAL_FOLDER/var/cache/" "$DEPLOY_FOLDER/var/cache" 

cd $DEPLOY_FOLDER"/"


make db-update
# priv cache update 2/2
php bin/console we:priv:reloadpoolcache
PRIVPOOLCACHESHANEW="$(sha1sum $PRIVPOOLCACHEFILE)"
echo $PRIVPOOLCACHESHANEW;
if [ "$PRIVPOOLCACHESHA" != "$PRIVPOOLCACHESHANEW" ]; then
  echo "  priv cache not same warming up admin priv cache"
  SECONDS=0
  php bin/console we:admin:privcachewarmup
  duration=$SECONDS
  MSG="  duration $(($duration / 60)) minutes and $(($duration % 60)) seconds"
  echo $MSG
fi

#make setup
rm -f var/cache/dev/appDevDebugProject*

CRONCONTENT="$(cat $DEPLOY_FOLDER/crontab)"
CRONCONTENT="${CRONCONTENT//__PATH__/$DEPLOY_FOLDER}"
echo "updating cron content"
echo "$CRONCONTENT"
echo "$CRONCONTENT" | crontab -


duration=$SECONDS
echo "done in $(($duration / 60)) minutes and $(($duration % 60)) seconds"
if [ "$NOTIFY_EMAIL" != false ]
	echo "$NOTIFY_MESSAGE" | mail -a"From: $NOTIFY_FROM" $NOTIFY_EMAIL
fi
